# DWMMPDSB #

### Summary ###

DWMMPDSB (Dynamic Window Manager Music Player Daemon Status Bar) displays MPD status (among other things) in dwm status bar.

![screenshot](scrot.png)

### Usage ###

Add dwmmpdsb to ~/.xinitrc.

### Installation ###

`make && make install`

### Dependencies and requirements ###

- libX11
- libasound
- libmpdclient

### Known bugs ###

- crashes when MPD is killed

### TODO ###

- fix bugs
- add options for customization
- cleanup and optimization
- add memory and network usage
- ~~add MPD progress bar (would require getting MPD status periodically)~~
- ~~add weather info (would require internet)~~

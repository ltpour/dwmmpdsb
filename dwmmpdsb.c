/* 
   DWMMPDSB: Dynamic Window Manager Music Player Daemon Status Bar

   Copyright 2018 Lasse Pouru

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <getopt.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>
#include <alsa/asoundlib.h>
#include <mpd/client.h>
#include <X11/Xlib.h>

#define PROGRAM_NAME "DWMMPDSB"
#define PROGRAM_VERSION "0.4"

/* Variables */
static const char *host;
static const struct option longopts[] = {
    {"help",    no_argument,       NULL, 'h'},
    {"port",    required_argument, NULL, 'p'},
    {"host",    required_argument, NULL, 's'},
    {"version", no_argument,       NULL, 'v'},
    {NULL,      0,                 NULL, 0 }};
static unsigned port;
static volatile sig_atomic_t quit = 0;
static volatile pid_t pid;

/* Functions */
static const char *bat_bar(int);
static const char *bat_path(void);
static pid_t daemonize(void);
static void help (char*);
static int mpd_connect(struct mpd_connection**);
static void parse_opts (int, char*[]);
static void termination_handler(int);
static void *update_bat_str(void*);
static void *update_cpu_str(void*);
static void *update_datetime_str(void*);
static void *update_mpd_str(void*);
static void *update_temp_str(void*);
static void *update_vol_str(void*);
static void version (char*);
static const char *vol_bar(int);
static void vol_to_vol_str(char*);
static void xsetroot(char*, Display*);

/* Main */
int main(int argc, char *argv[]) {
	
    /* Parse options */
    parse_opts(argc, argv);

    /* Open log */
    openlog(PROGRAM_NAME, LOG_ODELAY, LOG_DAEMON);

    /* Become daemon */
    pid = daemonize();

	/* Connect to the display */
	Display *display;
	if (!(display = XOpenDisplay(NULL))) {
		syslog(LOG_ERR, "%s: could not open display: %m", PROGRAM_NAME);
		closelog();
		exit(EXIT_FAILURE);
	}

	/* Default output strings */
	char bat_str[5] = "";
	char cpu_str[5] = "";
	char datetime_str[20] = "";
	char mpd_str[120] = "--";
	char temp_str[6] = "";
	char vol_str[5] = "";

	/* Create threads */
	pthread_t update_bat_thread, update_cpu_thread, update_temp_thread, update_mpd_thread, update_vol_thread, update_datetime_thread;
	pthread_t *threads[] = {&update_bat_thread, &update_cpu_thread, &update_temp_thread, &update_mpd_thread, &update_vol_thread, &update_datetime_thread};
	void *thread_routines[] = {update_bat_str, update_cpu_str, update_temp_str, update_mpd_str, update_vol_str, update_datetime_str};
	void *thread_args[] = {bat_str, cpu_str, temp_str, mpd_str, vol_str, datetime_str};
	for (size_t i = 0; i < sizeof(threads)/sizeof(threads[0]); i++) {
		errno = pthread_create(threads[i], NULL, thread_routines[i], thread_args[i]);
		if(errno) {
			syslog(LOG_ERR, "%s: error creating thread: %m", PROGRAM_NAME);
			quit = 2;
		}
	}
		
	/* Main loop */
	size_t i, output_size;
	while(!quit) {

		output_size = 0;
		for (i = 0; i < sizeof(threads)/sizeof(threads[0]); i++) {
			output_size += (strlen((char*)thread_args[i]) + 1); // elements + separators
		}
		output_size += 1; // newline

		/* Final output */
		char output[output_size]; // newline + separators
		memset (output, 0, sizeof output);
		for (i = 0; i < sizeof(threads)/sizeof(threads[0]); i++) {
			strncat(output, (char*)thread_args[i], strlen((char*)(thread_args[i]))); // elements + separators
			strncat(output, " ", 1);
		}
		xsetroot(output, display);

		sleep(1);
	}

	/* Join threads */
	for (i = 0; i < sizeof(threads)/sizeof(threads[0]); i++) {
		errno = pthread_join(*threads[i], NULL);
		if(errno) {
			syslog(LOG_ERR, "%s: error joining thread: %m", PROGRAM_NAME);
			quit = 2;
		}
	}

	/* Exit */
	XCloseDisplay(display);
    closelog();
	if (quit > 1) // 2 => error(s) occurred
		exit(EXIT_FAILURE);
	else
		exit(EXIT_SUCCESS);
}

/* Returns a symbol representing battery capacity */
static const char *bat_bar(int percent) {
	const char *symbols[] = { " ", "░", "▒", "▓", "█" };
	return (percent < 0) ? "" : symbols[percent / 23];
}

/* Returns the path to battery capacity file, if any */
static const char *bat_path(void) {
	if (access("/sys/class/power_supply/BAT0/capacity", F_OK ) != -1 ) {
		return "/sys/class/power_supply/BAT0/capacity";
	} else if (access("/sys/class/power_supply/BAT1/capacity", F_OK ) != -1 ) {
		return "/sys/class/power_supply/BAT1/capacity";
	} else
		return NULL;
}

/* Returns pid of new daemon */
static pid_t daemonize(void) {

    /* Fork and exit parent */
    pid_t pid = fork();
    if (pid < 0) {
		syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
		closelog();
		exit(EXIT_FAILURE);
    } else if (pid > 0)
		exit(EXIT_SUCCESS);

    /* Create a new SID for the child process */
    if (setsid() < 0) {
		syslog(LOG_ERR, "%s: could not create a session ID: %m", PROGRAM_NAME);
		closelog();
		exit(EXIT_FAILURE);
    }

    /* Set signal handlers */
    signal(SIGHUP, termination_handler);
    signal(SIGINT, termination_handler);
    signal(SIGTERM, termination_handler);
    signal(SIGSEGV, termination_handler);
    
    /* Fork and exit parent again */
    pid = fork();
    if (pid < 0) {
		syslog(LOG_ERR, "%s: could not fork: %m", PROGRAM_NAME);
		closelog();
		exit(EXIT_FAILURE);
    } else if (pid > 0)
		exit(EXIT_SUCCESS);
    
    /* Change the working directory to root */
    if ((chdir("/")) < 0) {
		syslog(LOG_ERR, "%s: could not change working directory: %m", PROGRAM_NAME);
		closelog();
		exit(EXIT_FAILURE);
    }

    /* Change the file mode mask */
    umask(0);

    /* Close all open file descriptors */
    for (int fd = sysconf(_SC_OPEN_MAX); fd > 0; fd--)
		close (fd);

    /* Return daemon pid */
    return getpid();
}

/* Prints help */
static void help (char* prgname) {
    printf("Usage: %s [OPTION]...\n", prgname);
    printf("Show MPD status (among other things) in dwm status bar.\n\n");
    printf("  -p, --port\tmpd port; defaults to MPD_PORT\n");
    printf("  -s, --host\tmpd host or socket;lts to MPD_HOST\n");
    printf("  -v, --version\tshow version information and exit\n");
    printf("  -h, --help\tshow this help and exit\n");
}

/* Connects to MPD server, returns 0 on success */
static int mpd_connect(struct mpd_connection **c) {
	int ret_val = 0;
	struct mpd_connection *connection = mpd_connection_new(host, port, 0);
    if (mpd_connection_get_error(connection) != MPD_ERROR_SUCCESS) {
		mpd_connection_free(connection);
		syslog(LOG_ERR, "%s: could not connect to MPD server: %m", PROGRAM_NAME);
		ret_val = 1;
    }
	*c = connection;
	return ret_val;
}

/* Parses options */
static void parse_opts (int argc, char* argv[]) {

    /* Set option flags */
    int c = 0, h = 0, p = 0, s = 0, v = 0;
    while((c = getopt_long(argc, argv, "d:hp:s:v", longopts, NULL)) != -1) {
		switch (c) {
		case 0:
			break;
		case 'h': // help
			h = 1;
			break;
		case 'p': // port
			port = atoi(optarg);
			p = 1;
			break;
		case 's': // host
			host = optarg;
			s = 1;
			break;
		case 'v': // version
			v = 1;
			break;
		default:
			printf("Try '%s --help' for more information.\n", argv[0]);
			exit(EXIT_FAILURE);
		}
    }

    /* Assign variables or display help or version information */
    if (h) {
		help(argv[0]);
		exit(EXIT_SUCCESS);
    }
    if (v) {
		version(argv[0]);
		exit(EXIT_SUCCESS);
    }
    if (!p)
		port = 0;
    if (!s)
		host = NULL;
    return;
}

/* Sets quit flag for exiting main loop */
static void termination_handler(int signum) {
    switch(signum) {
    case SIGINT:
    case SIGTERM:
    case SIGSEGV:
		quit = 1;
    default:
		break;
    }
}

/* Updates battery indicator every 30 seconds */
static void *update_bat_str(void *bat_str) {

	const char *bat = bat_path();
	FILE *fp;
	int capacity = -1;
	size_t bat_str_len;
	while(!quit) {
		
		if (bat) {
			fp = fopen(bat, "r");
			fscanf(fp, "%d", &capacity);
			fclose(fp);
			if (capacity > 100)
				capacity = 100;

			if (capacity < 0)
				bat_str_len = 3;
			else
				bat_str_len = 5;
			memset(bat_str, 0, bat_str_len);

			strncpy(bat_str, bat_bar(capacity), bat_str_len - 1);
			((char*)bat_str)[bat_str_len - 1] = '\n';
		}

		sleep(30);
	}
	
	pthread_exit(NULL);
}

/* Updates CPU load indicator once a second */
static void *update_cpu_str(void *cpu_str) {

	FILE *fp;
	long double a[4], b[4], avg;
	size_t cpu_str_len;	
	while(!quit) {

		fp = fopen("/proc/stat","r");
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
		fclose(fp);
		sleep(1);

		fp = fopen("/proc/stat","r");
		fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
		fclose(fp);

		avg = 100 * (((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3])));

		if (avg < 10)
			cpu_str_len = 3;
		else if (avg < 100)
			cpu_str_len = 4;
		else
			cpu_str_len = 5;	
		memset(cpu_str, 0, strlen(cpu_str));

		snprintf(cpu_str, cpu_str_len - 1, "%Lf", avg);
		strncat(cpu_str, "%", 2);

	}

	pthread_exit(NULL);
}

/* Updates time once a second */
static void *update_datetime_str(void *datetime_str) {

	time_t raw;
	while(!quit) {		
		time(&raw);
		snprintf(datetime_str, 20, "%s", ctime(&raw));

		sleep(1);
	}

	pthread_exit(NULL);
}

/* Updates MPD status once a second */
static void *update_mpd_str(void *mpd_str) {

    /* Connect to MPD server */
    struct mpd_connection *connection;
	int mpd_connected = mpd_connect(&connection) ? 0 : 1;

	/* File descriptor for polling MPD events */
	struct pollfd fds[1];
	if (mpd_connected) {
		mpd_send_idle(connection);
		fds[0].fd = mpd_connection_get_fd(connection);
		fds[0].events = POLLIN;
	}

	struct mpd_status *status;
	enum mpd_state state;
	char mpd_state_str[3] = "";
	struct mpd_song *song;
	size_t mpd_str_len;
	while(!quit) {

		/* Poll for MPD events if connected */
		if (mpd_connected) {
			if (poll(fds, 1, 1000)) {

				mpd_recv_idle(connection, 1);

				/* Send command list */
				mpd_command_list_begin(connection, true);
				mpd_send_status(connection);
				mpd_send_current_song(connection);
				mpd_command_list_end(connection);

				/* State */
				status = mpd_recv_status(connection);
				state = mpd_status_get_state(status);
				switch (state) {
				case MPD_STATE_PLAY:
					strncpy(mpd_state_str, ">>", 2);
					mpd_state_str[2] = '\0';
					break;
				case MPD_STATE_PAUSE:
					strncpy(mpd_state_str, "||", 2);
					mpd_state_str[2] = '\0';
					break;
				case MPD_STATE_STOP:
					strncpy(mpd_state_str, "><", 2);
					mpd_state_str[2] = '\0';
					break;
				default:
					strncpy(mpd_state_str, "!!", 2);
					mpd_state_str[2] = '\0';
				}

				/* Song */
				mpd_response_next(connection);
				song = mpd_recv_song(connection);
				if(song) {
					const char *artist = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0) ? mpd_song_get_tag(song, MPD_TAG_ARTIST, 0) : "";
					const char *title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0) ? mpd_song_get_tag(song, MPD_TAG_TITLE, 0) : "";

					/* Construct mpd str */
					mpd_str_len = strlen(mpd_state_str) + strlen(artist) + strlen(title) + 5; // newline + separators
					if (mpd_str_len > 118)
						mpd_str_len = 118;
					snprintf(mpd_str, mpd_str_len, "%s %s - %s", mpd_state_str, artist, title);

					mpd_song_free(song);
				}
				mpd_response_finish(connection);
				mpd_send_idle(connection);			
			}
		} else {
			/* Retry connecting */
			mpd_connected = mpd_connect(&connection) ? 0 : 1;
			if (mpd_connected) {
				mpd_send_idle(connection);
				fds[0].fd = mpd_connection_get_fd(connection);
				fds[0].events = POLLIN;
			}
			sleep (1); // wait for the second we would otherwise spend polling
		}
	}
	
	if (mpd_connected)
		mpd_connection_free(connection);

	pthread_exit(NULL);
}

/* Updates temperature once a second */
static void *update_temp_str(void *temp_str) {
	
	FILE *fp;
	long temp;
	size_t temp_str_len;
	while(!quit) {

		fp = fopen("/sys/class/thermal/thermal_zone0/temp","r");
		fscanf(fp,"%li", &temp);
		fclose(fp);
		temp /= 1000;

		if (temp < 10)
			temp_str_len = 4;
		else if (temp < 100)
			temp_str_len = 5;
		else
			temp_str_len = 6;	
		memset(temp_str, 0, strlen(temp_str));

		snprintf(temp_str, temp_str_len - 1, "%li", temp);
		strncat(temp_str, "°C", 3);

		sleep(1);
	}

	pthread_exit(NULL);
}

/* Updates volume indicator when volume changes */
static void *update_vol_str(void *vol_str) {
	
	struct pollfd fds;
	snd_ctl_event_t *event;
	snd_ctl_t *ctl;
	unsigned short revents;

	/* Initial volume */
	vol_to_vol_str((char*)vol_str);

	/* Connect to control device and subscribe to events */
	if (snd_ctl_open(&ctl, "default", SND_CTL_READONLY) < 0) {
		syslog(LOG_ERR, "%s: could not open ctl: %m", PROGRAM_NAME);
		quit = 2;
	}
	if (snd_ctl_subscribe_events(ctl, 1) < 0) {
		syslog(LOG_ERR, "%s: failed to subscribe events to ctl: %m", PROGRAM_NAME);
		quit = 2;
	}

	while (!quit) {

		/* Poll for events and update volume indicator on event */
		snd_ctl_poll_descriptors(ctl, &fds, 1);
		if (poll(&fds, 1, -1) > 0) {
			snd_ctl_poll_descriptors_revents(ctl, &fds, 1, &revents);
			if (revents & POLLIN) {
				snd_ctl_event_alloca(&event);
				if (snd_ctl_read(ctl, event) < 0) {
					syslog(LOG_ERR, "%s: error reading ctl events: %m", PROGRAM_NAME);
				} else {
					if (snd_ctl_event_get_type(event) == SND_CTL_EVENT_ELEM) {

						vol_to_vol_str((char*)vol_str);
					}
				}
			}
		}
	}
	snd_ctl_close(ctl);

	pthread_exit(NULL);
}

/* Prints program version */
static void version (char* prgname) {
    printf("%s %s\n", PROGRAM_NAME, PROGRAM_VERSION);
    printf("Copyright 2018 Lasse Pouru\n");
    printf("License GPLv3: GNU GPL version 3 <http://gnu.org/licenses/gpl.html>\n");
}

/* Returns a symbol representing volume */
static const char *vol_bar(int percent) {
	const char *symbols[] = { " ", "_", "▁", "▂", "▃", "▄", "▅", "▆", "▇", "█" };
	return (percent < 0) ? "M" : symbols[percent / 11];
}

/* Updates volume indicator */
static void vol_to_vol_str(char *vol_str) {
	
	int on = 1;
	long min, max, percent, vol = 0;
	size_t vol_str_len;
	snd_mixer_t *handle;
	snd_mixer_selem_id_t *sid;
	snd_mixer_elem_t* elem;
	
	snd_mixer_open(&handle, 0);
	snd_mixer_attach(handle, "default");
	snd_mixer_selem_register(handle, NULL, NULL);
	snd_mixer_load(handle);
	snd_mixer_selem_id_alloca(&sid);
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, "Master");
	elem = snd_mixer_find_selem(handle, sid);
	if (!elem) {
		snd_mixer_selem_id_set_name(sid, "PCM"); // show PCM volume if Master doesn't exist
		elem = snd_mixer_find_selem(handle, sid);
	}		
	if (snd_mixer_selem_has_playback_switch(elem)) {
		snd_mixer_selem_get_playback_switch(elem, 0, &on);
	}
	snd_mixer_selem_get_playback_volume_range(elem, &min, &max);
	snd_mixer_selem_get_playback_volume(elem, 0, &vol);
	snd_mixer_close(handle);

	if (on) {
		percent = (double)vol / max * 100;
		vol_str_len = 5;
	}
	else {
		percent = -1;
		vol_str_len = 4;
	}
		
	memset(vol_str, 0, vol_str_len);
	strncpy(vol_str, vol_bar(percent), vol_str_len - 1);
	vol_str[vol_str_len - 1] = '\n';
}

/* Sets WM_NAME of the root window (= contents of the dwm statusbar) */
static void xsetroot(char *str, Display *display) {
	XStoreName(display, DefaultRootWindow(display), str);
	XSync(display, False);
}
